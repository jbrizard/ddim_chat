// Connexion au socket
var socket = io.connect(':8090');

// Demande un pseudo et envoie l'info au serveur
var name = prompt('Quel est votre pseudo ?', 'User'+Math.floor(Math.random() * 9999));
socket.emit('user_enter', name);

// Gestion de l'historique
socket.on('history', setHistoryMessages);

// Gestion des événements diffusés par le serveur
socket.on('new_message', receiveMessage);
socket.on('delete_user_from_list', deleteUserFromList);
socket.on('load_users_list', loadUsersList);

// Action quand on clique sur le bouton "Envoyer"
$('#send-message').click(sendMessage);

// Gestion des événements diffusés par le serveur
socket.on('scribbl_receive_pos', receiveScribbl);
socket.on('receive_erase', erase);
socket.on('receive_color', changeColor);

// Action quand on appuye sur la touche [Entrée] dans le champ de message (= comme Envoyer)
$('#message-input').keyup(function(evt)
{
	if (evt.keyCode == 13) // 13 = touche Entrée
		sendMessage();
});

$('#filetoupload').change(function(){
	
	var fileData = $("#filetoupload")[0].files[0];
	var fileName = fileData.name;
	var fileType = fileData.type;
	var tabFiles = {
		data: fileData, 
		name: fileName,
		type: fileType
	}

	socket.emit('file', tabFiles);	
});

if (Notification.permission !== "granted")
        Notification.requestPermission();

/**
 * Envoi d'un message au serveur
 */
function sendMessage()
{
	// Récupère le message, puis vide le champ texte
	var input = $('#message-input');
	var message = input.val();
	input.val('');

	// On n'envoie pas un message vide
	if (message == '')
		return; 
	
	// Envoi le message au serveur pour broadcast
	socket.emit('message', message);
}

/**
 * Affichage d'un message reçu par le serveur
 */
function receiveMessage(data)
{
	console.log('receive', data);
	if (typeof(data.new_avatar) == 'undefined')
		data.new_avatar = 'jdoe.jpg';
	
	$('#chat #messages').append(
		'<div class="message">'
		+ '<div class="avatar" style="background-image:url('+ data.new_avatar + '?' + Date.now() +');"> </div>'
			+ '<span class="user">' + data.name + '</span>  '
			+ '<p class="contenu_message">' + data.message +'</p>  '
	    + '</div>'
	)
	.scrollTop(function(){ return this.scrollHeight });  // scrolle en bas du conteneur
	
	if (!data.isMe)
	{
		trySendNotification(data);
	}
}

function trySendNotification(data)
{
	if (!("Notification" in window) || Notification.permission == 'denied')
		return;
	
	var message = data.message;
	if (message.length > 40) message = message.substr(0, 40) + '...';
	new Notification(data.name + ' : ' + message);
}

function deleteUserFromList(data)
{
	let id = data.id;
	let element = document.getElementById(id);

	if( element != null )
	{
		element.remove();
	}
}

function loadUsersList(data)
{
	var list = $("#users-list-container");
	list.empty();

	for( let user in data )
	{
		let name = data[user].name;
		let fullPseudo = data[user].name;
		let avatar = data[user].avatar || "jdoe.jpg";
		
		if (name.length >= 13 )
		{
			name = textMinimizer(data[user].name);
		}
		
		var listItem = $(`<div class="users-list-item" id="${data[user].id}" title="${fullPseudo}">
							<div class="users-list-item-picture" style="background-image:url(${avatar}?${Date.now()});"></div>
							<div class="users-list-item-name">${name}</div>
							<div class="users-list-item-status" style="display: none"><img src="lapiz.png" width="30" height="30px"></div>
						</div>`);
		list.append(listItem);
	}
}

function textMinimizer( str )
{
	let content = str.substr(0, 10);

	return content + `<span fullPseudo='${str}' class='showFullName'>...</span>`;
}

function receiveScribbl(data)
{
	canvas = document.getElementById('can');
	ctx = canvas.getContext("2d");
	w = canvas.width;
	h = canvas.height;
	ctx.beginPath();
	ctx.moveTo(data.prevX, data.prevY);
	ctx.lineTo(data.currX, data.currY);
	ctx.stroke();
	ctx.closePath();
}

function erase(effacer)
{
	if(effacer)
	{
		ctx.clearRect(0, 0, w, h);
	}
}

function changeColor(data)
{
	ctx.strokeStyle = data.colorX;
	ctx.lineWidth = data.colorY;
}

$('#btn-help').click(function()
{
	$(this).hide();
	$(this).siblings('.inner').show();
});

$('#btn-close-help').click(function()
{
	$(this).parent('.inner').hide();
	$('#btn-help').show();
});

$('#smiley').click(clickSmiley);
$('span').click(sendSmiley);

function clickSmiley(){
	$('#smiley-list').toggle();
}

function sendSmiley(){
	var smiley =  $(this).data('smiley');
	
	if(typeof(smiley) == undefined)
		return;
	;
	socket.emit('message', smiley);
}

$('#bulle-button').click(function()
{
	socket.emit('message', 'bulle');
});

$('#scribbl-button').click(function()
{
	socket.emit('message', 'scribbl');
});

/**
 * Réafficher tout les anciens messages via l'historique
 */
function setHistoryMessages(data)
{
	for(let i=0; i< data.history.length; ++i)
	{
		var msg = data.history[i];
		msg.isMe = true; // <- pour éviter les notifications
		receiveMessage(msg);
	}
}


/**
 * Vérifie si une variable est "set" (pas undefined)
 */
function isset(val)
{
	return typeof(val) != 'undefined';
}