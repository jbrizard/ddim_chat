socket.on('scribbl_start', start_scribbl);
socket.on('scribbl_test', winner);
$('.scribbl_color').click(color);
$('#erase-canvas').click(erase);


var canvas, ctx, mouseIsDown = false,
    prevX = 0,
    currX = 0,
    prevY = 0,
    currY = 0,
    dot_flag = false;

var x = "black",
    y = 2;

function winner(offline,word)
{
    $(".message:last-child").css('color', '#62BD06');
    $(".message:last-child").css('font-weight', 'bold');
    $(".message:last-child").append('<br/>Bravo vous avez trouvé le mot !');

    if(offline)
    {
        $(".scribbl").hide();
        $("#erase-canvas").hide();
        $(".scribbl_color").hide();
        $('#random_word').val(' ');
        erase();
    }
    
}

function start_scribbl(isPlayer, word, online = false)
{
    $(".scribbl").show();

    //juste pour le joueur 
    if (isPlayer && online)
    {
        $("#erase-canvas").show();
        $(".scribbl_color").show();
        $('#random_word').val(word);
        
        canvas = document.getElementById('can');
        ctx = canvas.getContext("2d");
        w = canvas.width;
        h = canvas.height;

        canvas.addEventListener("mousemove", function (e) {
            if (mouseIsDown) {
                prevX = currX;
                prevY = currY;
                currX = e.clientX - canvas.offsetLeft;
                currY = e.clientY - canvas.offsetTop;

                socket.emit('scribbl_send_pos', {
                    prevX:prevX,
                    prevY:prevY,
                    currX:currX,
                    currY:currY,
                    word:word
                });
            }
        }, false);

        canvas.addEventListener("mousedown", function (e)
        {
            prevX = currX;
            prevY = currY;
            currX = e.clientX - canvas.offsetLeft;
            currY = e.clientY - canvas.offsetTop;
            mouseIsDown = true;
            
        }, false);

        canvas.addEventListener("mouseup", function (e) {
            mouseIsDown = false;
        }, false);

        canvas.addEventListener("mouseout", function (e) {
            mouseIsDown = false;
        }, false);
    }
}


function erase()
{
    var effacer = true;
    socket.emit('send_erase', effacer);
}

function color(obj) {
    switch (obj.id) {
        case "green":
            x = "green";
            break;
        case "blue":
            x = "blue";
            break;
        case "red":
            x = "red";
            break;
        case "yellow":
            x = "yellow";
            break;
        case "orange":
            x = "orange";
            break;
        case "black":
            x = "black";
            break;
        case "white":
            x = "white";
            break;
    }
    if (x == "white") y = 14;
    else y = 2;
    
    socket.emit('send_color', {x:x,y:y});
}
