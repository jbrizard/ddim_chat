// Chargement des dépendances
var express = require('express');	// Framework Express
var http = require('http');		// Serveur HTTP
var ioLib = require('socket.io');	// WebSocket
var ent = require('ent');		// Librairie pour encoder/décoder du HTML
var path = require('path');		// Gestion des chemins d'accès aux fichiers

// Chargement des modules perso
var daffy = require('./modules/daffy.js');
var quizz = require('./modules/quizz.js');
var jack = require('./modules/sparrow.js');
var bulles = require('./modules/bulles.js');
var smileys = require('./modules/smileys.js');
var easterEggs = require('./modules/easter-eggs.js');
var blague = require('./modules/blague.js');
var basket = require('./modules/basket.js');
var wizz = require('./modules/wizz.js');
var avatarsModule = require('./modules/avatars.js');
var typing = require('./modules/feedback.js');
var meteo = require('./modules/meteo.js');
var upload = require('./modules/upload.js');
var jukebox = require('./modules/jukebox.js');
var youtubeBot = require('./modules/youtubeBot.js');
var history = require('./modules/history.js');
var bimbamboom = require('./modules/bimbamboom.js');
var gif = require('./modules/gif.js');
var scribbl = require('./modules/scribbl.js');

// Initialisation du serveur HTTP
var app = express();
var server = http.createServer(app);

let users = {};

// Initialisation du websocket
var io = ioLib.listen(server)

// Traitement des requêtes HTTP (une seule route pour l'instant = racine)
app.get('/', function(req, res)
{
	res.sendFile(path.resolve(__dirname + '/../client/chat.html'));
});

// Traitement des fichiers "statiques" situés dans le dossier <assets> qui contient css, js, images...
app.use(express.static(path.resolve(__dirname + '/../client/assets')));
app.use(express.static(path.resolve(__dirname + '/../client/assets/upload')));

// Initialisation du module Basket & Bulles
basket.init(io);
bulles.init(io);

// Gestion des connexions au socket
io.sockets.on('connection', function(socket)
{
	console.log('connection ' + socket.id);
	// Insère le user dans le tableau
	users[socket.id] = { id:socket.id, name: '...' };
	io.sockets.emit('load_users_list', users);
	
	basket.addClient(socket);
	
	io.sockets.emit('easter_egg',{
		name:'user_enter.mp3',
	});
	
	// Réception d'avatar
	socket.on('new_avatar', function(avatar)
	{
		avatarsModule.handleAvatars(avatar, socket);
	
		if (typeof(users[socket.id]) != 'undefined')
			users[socket.id].avatar = socket.avatar;

		io.sockets.emit('load_users_list', users);
	});

	// Arrivée d'un utilisateur
	socket.on('user_enter', function(name)
	{
		// Stocke le nom de l'utilisateur dans l'objet socket
		socket.name = name;
		
		console.log('user_enter ' + socket.id);
		
		// Met à jour le pseudo dans la liste des users
		users[socket.id].name = socket.name;
		io.sockets.emit('load_users_list', users);
		
		history.getHistory(io);
		
		io.sockets.emit('easter_egg',{
			name:'user_enter.mp3',
		});
	});

	//Déconnexion d'utilisateur
	socket.on('disconnect', function(avatar)
	{
		avatarsModule.deleteAvatars(avatar, socket);
		
		let user = users[socket.id];
		io.sockets.emit('delete_user_from_list', user);
		delete users[socket.id];
		
		io.sockets.emit('easter_egg',{
			name:'userdisconnect.mp3',
		});
	});

	// Réception d'un message
	socket.on('message', function(message)
	{	
		history.addToHistory(socket.name, message);

		youtubeBot.youtubeSearch(io, message);
		// Par sécurité, on encode les caractères spéciaux
		message = ent.encode(message);

		message = smileys.handleSmileys(message);

		// Transmet le message à tous les utilisateurs (broadcast)
		socket.emit('new_message', {name:socket.name, message:message, new_avatar:socket.avatar, isMe:true });
		socket.broadcast.emit('new_message', {name:socket.name, message:message, new_avatar:socket.avatar, isMe:false });
		
		// Transmet le message au module Daffy (on lui passe aussi l'objet "io" pour qu'il puisse envoyer des messages)
		daffy.handleDaffy(io, message);
		easterEggs.handleEasterEggs(io, message);
		jack.handleJack(io, message, ent);
		bulles.handleBulles(message, ent);
		blague.handleBlague(io, message);
		quizz.handleQuizz(io, message);
		wizz.handleWizz(io, message);
		meteo.handleMeteo(io, message);
		jukebox.handleJukebox(io, message);
		bimbamboom.handleBimbamboom(io, message);
		gif.handleGif(io,message);
		scribbl.handleScribbl(io, socket, message);
	});

	socket.on('scribbl_send_pos', function(data)
	{
		io.sockets.emit('scribbl_receive_pos', {name:socket.name, currX:data.currX, currY:data.currY, prevX:data.prevX, prevY:data.prevY, word:data.word});
	});	

	socket.on('send_erase', function(data)
	{
		io.sockets.emit('receive_erase', {name:socket.name, effacer:data.effacer});
	});

	socket.on('send_color', function(data)
	{
		io.sockets.emit('receive_color', {name:socket.name, colorX:data.x, colorY:data.y});
	});

	//User écrit
	socket.on("is_typing", function(isTyping)
	{
		
		typing.handleTyping(io,socket,isTyping);
	});
	
	// Réception d'un fichier
	socket.on('file', function(tabFiles)
	{
		upload.handleUpload(io, tabFiles, socket);
	});

	socket.on('clickBulle', function(id)
	{
		bulles.destroyBulle(id, socket);
	});

});

// Lance le serveur sur le port 8090 (http://localhost:8090)
server.listen(8090);