/*
 * Nom : Poti blagueur !
 * Description : Ce module ne fait pas grand chose... quand on appelle Poti blagueur, il raconte une blague !
 * Auteur(s) : Maëva MEZZASALMA Camille GROSJEAN
 */

// Définit les méthodes "publiques" (utilisation à l'extérieur du module)
module.exports =  {
	handleBlague: handleBlague // permet d'appeler cette méthode dans server.js -> blague.handleBlague(...)
}

/**
 * Lorsqu'on appelle Poti blagueur, il répond...
 */

 function getRequest(io, message)
 {
	 // console.log('getRequest');
    const request = require('request');
	
    request('https://blague.xyz/joke/random', { json: true }, (err, res, body) =>
	{
		if (err) { return console.log(err); }
		// console.log(body.joke.question);
		// console.log(body.joke.answer);

		io.sockets.emit('new_message',
		{
			name:'Pouet!!',
			message: body.joke.question
		});
		
		setTimeout(afficheRep, 1000, io, message, body);
	});
 }

function afficheRep(io, message, body)
{
	io.sockets.emit('new_message',
	{
		name:'Pouet!!',
		message: body.joke.answer
	});
};

function handleBlague(io, message)
{
	// Passe le message en minuscules (recherche insensible à la casse)
	message = message.toLowerCase();
	
	// Est-ce qu'il contient une référence à Poti blagueur ?
	if (message.includes('poti blagueur') || message.includes('blague'))
	{
		getRequest(io, message)
	}
}
