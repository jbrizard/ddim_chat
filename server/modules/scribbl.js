//permet de faire bouger la fenêtre

/*
 * Nom : Wizz
 * Description : Fait vibrer l'écran quand on entre :wizz dans le chat.
 * Auteur(s) : Rioky Ya nak, Dylan Louvet
 */
var ent = require('ent');
module.exports =  {
	handleScribbl: handleScribbl // permet d'appeler cette méthode dans server.js -> daffy.handleDaffy(...)
}

var word;
var gameOn = true;

function handleScribbl(io, socket ,message) {
    // Passe le message en minuscules (recherche insensible à la casse)
    message = message.toLowerCase();
    // Est-ce qu'il contient une référence Scribbl ?
    console.log('includes');
    
    if (message.includes('scribbl'))
    {
        if(gameOn)
        {
            gameOn = false;
            // Si oui, envoie la réponse de Scribbl...
            var array_word = ['piscine', 'ordinateur', 'intersection', 'maison', 'squelette', 'avion','missile','tracteur','video-projecteur','clavier','pokemon','chargeur','dragon','petanque','parasol','voile','universite','divia','dijon','sonic','mario','lezard','diplodocus','telephone','ecran','windows','atheneum','arret de bus','en surpoids','vomir','jeter','game-boy',];
            var random = Math.floor(Math.random() * Math.floor(array_word.length));
            word = array_word[random];

            socket.emit('scribbl_start',true, word, true);
            socket.broadcast.emit('scribbl_start', false, word, true);
        }
            
    }

    if(message == word)
    {
        io.sockets.emit('scribbl_test', true, word);
        gameOn = true;
        word="";
    }
}